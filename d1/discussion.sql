-- Add five artists

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Fearless",
	"2008-1-1",
	4
);

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Red",
	"2012-1-1",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Fearless",
	246,
	"Pop rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Love Story",
	213,
	"Country Pop",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"State of Grace",
	253,
	"Rock, alternative rock, arena rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Red",
	204,
	"Country",
	5
);

-- Lady Gaga

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"A Star is Born",
	"2018-1-1",
	5
);

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Born This Way",
	"2011-1-1",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Black Eyes",
	151,
	"Rock",
	8
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Shallow",
	201,
	"Country,rock, folk rock",
	8
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Born This Way",
	252,
	"Electropop",
	9
);

-- Justin Bieber

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Purpose",
	"2015-1-1",
	6
);

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Believe",
	"2012-1-1",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Sorry",
	132,
	"Dancehall",
	10
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Boyfriend",
	251,
	"Pop",
	11
);

-- [SECTION] Advanced Selects

-- Exclude records

SELECT * FROM songs where id != 11;

--Greater than

SELECT * FROM songs where id > 10;

--Greater than or equal

SELECT * FROM songs where id >= 10;

--Less than

SELECT * FROM songs where id < 5;

--Less than or equal

SELECT * FROM songs where id <= 5;

-- GET specifics IDs (OR)
SELECT * FROM songs where genre ="Pop" or ID = 2;

-- GET specifics IDs (IN)
SELECT * FROM songs where genre IN ("Pop","Rock");

-- Combining conditions
SELECT * FROM songs WHERE album_id = 2 AND id < 5;

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "s%"; -- partial match with the first character
SELECT * FROM songs WHERE song_name LIKE "%y"; -- partial match with the last character
SELECT * FROM songs WHERE song_name LIKE "%a%"; -- partial match from the middle

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC; -- Sorted by ascending order
SELECT * FROM songs ORDER BY song_name DESC; -- Sorted by descending order
SELECT * FROM songs ORDER BY RAND(); -- Random order

-- Limiting record output
SELECT * FROM songs LIMIT 3;

-- Getting distinct records
SELECT DISTINCT genre FROM songs;

-- [SECTION] Table Joins

-- Combine artists and albums tables
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table
SELECT artists.name,albums.album_title,songs.song_name FROM artists 
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;


--Using joins, display a single table ordered Song Name -> album name -> artist name of all Lady Gaga's songs. Show only the song name, album name and artist name

SELECT songs.song_name,albums.album_title,artists.name FROM songs
JOIN albums ON albums.id = songs.album_id
JOIN artists ON albums.artist_id = artists.id WHERE artists.name = "Lady Gaga";

SELECT * FROM artists
	LEFT JOIN albums on artists.id = albums.artist_id;

SELECT * FROM artists
	RIGHT JOIN albums on artists.id = albums.artist_id;	
